package com.lofitskyi.pos;

import com.lofitskyi.pos.controller.ApplicationController;
import com.lofitskyi.pos.service.PersonInfoService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

@SpringBootApplication
public class Cs51posApplication {

    public static void main(String[] args) {
        SpringApplication.run(Cs51posApplication.class, args);
    }

    @Bean(name = "personInfoService")
    PersonInfoService getPersonInfoService() {
        RmiProxyFactoryBean rmiProxyFactory = new RmiProxyFactoryBean();
        rmiProxyFactory.setServiceUrl("rmi://localhost:1099/PersonInfoService");
        rmiProxyFactory.setServiceInterface(PersonInfoService.class);
        rmiProxyFactory.afterPropertiesSet();
        return (PersonInfoService) rmiProxyFactory.getObject();
    }

}
