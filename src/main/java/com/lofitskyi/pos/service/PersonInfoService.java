package com.lofitskyi.pos.service;

import java.util.List;
import java.util.Set;

public interface PersonInfoService {
    List<String> getAllPersons();
    Set<String> getAllTasksForPerson(String username);
}
