package com.lofitskyi.pos.controller;

import com.lofitskyi.pos.service.PersonInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
public class ApplicationController {

    private final PersonInfoService personInfoService;

    @Autowired
    public ApplicationController(PersonInfoService personInfoService) {
        this.personInfoService = personInfoService;
    }

    @GetMapping(value = "/list")
    public List<String> getAllPersons() {
        return personInfoService.getAllPersons();
    }

    @GetMapping(value = "/tasks/{username}")
    public Set<String> getTasksForPerson(@PathVariable String username) {
        return personInfoService.getAllTasksForPerson(username);
    }
}
