package com.lofitskyi.cs51_pos_remote;

import com.lofitskyi.cs51_pos_remote.entity.Person;
import com.lofitskyi.cs51_pos_remote.entity.Task;
import com.lofitskyi.cs51_pos_remote.repository.PersonRepository;
import com.lofitskyi.cs51_pos_remote.service.PersonInfoService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiServiceExporter;

import java.util.stream.Stream;

@SpringBootApplication
public class Cs51PosRemoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(Cs51PosRemoteApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(PersonRepository personRepository) {
		return args -> {
            Class<PersonInfoService> serviceInterface = PersonInfoService.class;
            System.out.println(serviceInterface.getSimpleName());
			Person eduard = new Person("Eduard");
			Person aleksandra = new Person("Aleksandra");
			Person roman = new Person("Roman");

			eduard.addTask(new Task("Clean up flat"));
			eduard.addTask(new Task("Make dinner"));

			aleksandra.addTask(new Task("jump off window"));
			aleksandra.addTask(new Task("wake up at 4:35"));
			aleksandra.addTask(new Task("play tennis"));

			roman.addTask(new Task("Learn Swift"));

			Stream.of(eduard, aleksandra, roman)
					.forEach(personRepository::save);
		};
	}

	@Bean
	RmiServiceExporter exporter(PersonInfoService implementation) {

		// Expose a service via RMI. Remote obect URL is:
		// rmi://<HOST>:<PORT>/<SERVICE_NAME>
		// 1099 is the default port

		Class<PersonInfoService> serviceInterface = PersonInfoService.class;
		RmiServiceExporter exporter = new RmiServiceExporter();
		exporter.setServiceInterface(serviceInterface);
		exporter.setService(implementation);
		exporter.setServiceName(serviceInterface.getSimpleName());
		exporter.setRegistryPort(1099);
		return exporter;
	}
}
