package com.lofitskyi.cs51_pos_remote.repository;

import com.lofitskyi.cs51_pos_remote.entity.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {
}
