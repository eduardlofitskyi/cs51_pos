package com.lofitskyi.cs51_pos_remote.repository;

import com.lofitskyi.cs51_pos_remote.entity.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {

    Person findByUsername(String username);
}
