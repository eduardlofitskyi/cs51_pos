package com.lofitskyi.cs51_pos_remote.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class Task implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    private String description;

    @NonNull
    private LocalDateTime createDttm;
    private LocalDateTime deadlineDttm;

    @NonNull
    private Boolean isDone;

    public Task(String description) {
        this(description, false);
    }

    public Task(String description, boolean isDone) {
        this(description, null, isDone);
    }

    public Task(String description, LocalDateTime deadlineDttm, boolean isDone) {
        this.description = description;
        this.deadlineDttm = deadlineDttm;
        this.isDone = isDone;
        this.createDttm = LocalDateTime.now();
    }
}
