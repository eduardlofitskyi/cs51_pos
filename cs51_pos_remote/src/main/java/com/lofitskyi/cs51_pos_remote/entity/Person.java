package com.lofitskyi.cs51_pos_remote.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter @Setter
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@ToString
public class Person implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    private String username;

    @OneToMany(cascade= CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Task> tasks = new HashSet<>();

    public boolean addTask(Task task) {
        return this.tasks.add(task);
    }
}
