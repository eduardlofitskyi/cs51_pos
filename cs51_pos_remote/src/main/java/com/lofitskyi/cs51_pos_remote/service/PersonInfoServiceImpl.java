package com.lofitskyi.cs51_pos_remote.service;

import com.google.common.collect.Lists;
import com.lofitskyi.cs51_pos_remote.entity.Person;
import com.lofitskyi.cs51_pos_remote.entity.Task;
import com.lofitskyi.cs51_pos_remote.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PersonInfoServiceImpl implements PersonInfoService{

    private final PersonRepository personRepository;

    @Autowired
    public PersonInfoServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public List<String> getAllPersons() {
        return Lists.newArrayList(personRepository.findAll()).stream().map(Person::toString).collect(Collectors.toList());
    }

    @Override
    public Set<String> getAllTasksForPerson(String username) {
        return personRepository.findByUsername(username).getTasks().stream().map(Task::toString).collect(Collectors.toSet());
    }
}
