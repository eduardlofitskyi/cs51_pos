package com.lofitskyi.cs51_pos_remote.service;

import com.lofitskyi.cs51_pos_remote.entity.Person;
import com.lofitskyi.cs51_pos_remote.entity.Task;

import java.util.List;
import java.util.Set;

public interface PersonInfoService {
    List<String> getAllPersons();
    Set<String> getAllTasksForPerson(String username);
}
